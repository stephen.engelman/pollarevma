//
// Created by Stephen Engelman on 2/26/23.
//
#pragma once
#include <SDL2/SDL.h>
//#include <pixman-1/pixman.h>
#include <queue>

#define FRAME_DELAY 0

using namespace std;

struct NDISourceNum {
    const NDIlib_source_t* p_sources;
    uint32_t num;
};

static SDL_AudioDeviceID audio_dev;
static SDL_AudioSpec * wanted;
bool isAudioRunning = false;
static uint32_t poll_num = 0;

struct SDLDisplay {
public:
    SDL_Event event{};
    SDL_Window *sdl_window{};
    SDL_Renderer *sdl_renderer{};
    SDL_Texture *sdl_texture{};
    SDL_Surface *sdl_surface{};
    SDL_PixelFormatEnum sdl_pixel_format = SDL_PIXELFORMAT_UNKNOWN;
    uint32_t *pixels{};
    uint32_t pixels_xres{};
    uint32_t pixels_yres{};
    bool isRunning = false;
    uint32_t * pixel_screen{};
    std::queue<uint32_t *> screens;
    bool shutdown_thread = false;
    uint16_t frame_delay = FRAME_DELAY;

    SDLDisplay() = default;

    ~SDLDisplay() {
        this->OnExit();
    }

    bool init_display(uint32_t xres, uint32_t yres, uint32_t * pixels_data, const char  * name) {
        printf("%s%s\n", __FILE__, " starting...");
        this->pixels_xres = xres;
        this->pixels_yres = yres;
        printf("Initializing display: xres: %d  yes: %d\n", xres, yres);
        printf("%s%s\n", __FILE__, " SDL_Init() PASS");
        this->sdl_window = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)xres, (int)yres,
                                      SDL_WINDOW_SHOWN);
//        SDL_SetWindowBordered(this->sdl_window, SDL_FALSE);
//        SDL_SetWindowFullscreen(this->sdl_window, SDL_WINDOW_FULLSCREEN);
        if (this->sdl_window != nullptr) {
            printf("%s%s\n", __FILE__, " SDL_CreateWindow() PASS");
            this->sdl_renderer = SDL_CreateRenderer(this->sdl_window, -1, 0);
        } else {
            printf("%s%s\n", __FILE__, " SDL_CreateWindow() fail!");
            return false;
        }
        if (this->sdl_renderer != nullptr) {
            printf("%s%s\n", __FILE__, "SDL_CreateRenderer() PASS");
            SDL_RenderClear(this->sdl_renderer);
            SDL_RenderPresent(this->sdl_renderer);
        } else {
            printf("%s%s\n", __FILE__, " SDL_CreateRenderer fail!");
            return false;
        }
        // Initializing data
        this->sdl_surface = SDL_GetWindowSurface(this->sdl_window);
        if (this->sdl_surface == nullptr) {
            printf("%s%s\n", __FILE__, " SDL_CreateRGBSurface fail!");
            return false;
        }
        this->pixels = pixels_data;
        sdl_texture = SDL_CreateTexture(this->sdl_renderer, this->sdl_pixel_format, SDL_TEXTUREACCESS_STREAMING, (int)xres, (int)yres);
        if (this->sdl_texture == nullptr) {
            printf("%s%s\n", __FILE__, " SDL_CreateTextureFromSurface fail!");
            return false;
        }
        printf("%s%s\n", __FILE__, " function PASS");
        this->isRunning = true;

        //TODO: kick off SDL event loop thread for this display

        return true;
    }

    static bool init_audio(int freq, uint8_t channels, uint16_t samples) {
        /* Set the audio format */
        wanted = new SDL_AudioSpec;
        wanted->freq = freq;
        wanted->format = AUDIO_S16;
        wanted->channels = channels;    /* 1 = mono, 2 = stereo */
        wanted->samples = samples * 4;  /* Good low-latency value for callback */
        wanted->callback = nullptr; // Using SDL push method with  SDL_QueueAudio( and SDL_DequeueAudio
        wanted->userdata = nullptr;

        /* Open the audio device, forcing the desired format */
        audio_dev = SDL_OpenAudioDevice(nullptr, 0, wanted, nullptr, SDL_AUDIO_ALLOW_ANY_CHANGE);
        if ( !audio_dev ) {
            fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
            return(false);
        }
        printf("SDL_OpenAudio successfully opened!!\n");
        SDL_PauseAudioDevice(audio_dev, 0);
        isAudioRunning = true;
        return true;
    }

    void update_display(uint32_t * pdata, uint32_t xres, uint32_t yres) {
        // TODO You know this is insane. You need to check res change and then use SDL functions to change geometry,
        //      otherwise this is ripe for segfault.
        if (isAudioRunning) {
            pixel_screen = new uint32_t[yres * xres * 4];
            memcpy(pixel_screen, pdata, xres * yres * 4);
            screens.push(pixel_screen);
            if (screens.size() >= frame_delay) {
                this->pixels_xres = xres;
                this->pixels_yres = yres;
                auto * front = screens.front();
                this->pixels = front;
                this->update_display();
                screens.pop();
                delete[] front;
            }
        } else {
            this->pixels_xres = xres;
            this->pixels_yres = yres;
            this->pixels = pdata;
            this->update_display();
        }
//        cout << "SDL Polling... " << poll_num++ << endl;
// TODO: Put this into a separate thread - do this on initialization
        SDL_PollEvent(&event);
        if (event.key.keysym.sym == SDLK_q) {
            this->OnExit();
            this->shutdown_thread = true;
            cout << "SDL Polling: q" << poll_num << endl;
        } else if (event.key.keysym.sym == SDLK_UP) {
            this->frame_delay++;
            cout << "++ frame_delay=" << dec << this->frame_delay << endl;
        } else if (event.key.keysym.sym == SDLK_DOWN) {
            if (this->frame_delay > 0)
                this->frame_delay--;
            cout << "-- frame_delay=" << dec << this->frame_delay << endl;
        } else if (event.key.keysym.sym == SDLK_f) {
            cout << "Toggling to fullscreen" << endl;
            SDL_SetWindowBordered(this->sdl_window, SDL_FALSE);
            SDL_SetWindowFullscreen(this->sdl_window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        } else if (event.key.keysym.sym == SDLK_m) {
            cout << "Toggling to maximized" << endl;
            SDL_SetWindowBordered(this->sdl_window, SDL_TRUE);
            SDL_SetWindowFullscreen(this->sdl_window, SDL_WINDOW_MAXIMIZED);
        } else if (event.key.keysym.sym == SDLK_w) {
            cout << "Toggling to windows" << endl;
            SDL_SetWindowBordered(this->sdl_window, SDL_TRUE);
            SDL_SetWindowFullscreen(this->sdl_window, SDL_WINDOW_SHOWN);
        }
            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
        event = SDL_Event{};
    }

    void update_display() const {
        this->sdl_surface->pixels = this->pixels;
        SDL_UpdateTexture(this->sdl_texture, nullptr, this->sdl_surface->pixels, (int)(this->pixels_xres * sizeof(uint32_t)));
        SDL_RenderCopy(this->sdl_renderer, this->sdl_texture, nullptr, nullptr);
        SDL_RenderPresent(this->sdl_renderer);
    }

    void OnExit() {
        cout << "Shutting down SDL2..." << endl;
        SDL_PauseAudioDevice(audio_dev, 1);
        SDL_CloseAudio();
        // Tidy up SDL2 stuff
        SDL_DestroyRenderer(this->sdl_renderer);
        SDL_DestroyWindow(this->sdl_window);
        this->sdl_window = nullptr;
        SDL_Quit();
    }
};
