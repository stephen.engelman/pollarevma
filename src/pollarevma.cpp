#include <cstdio>
#include <chrono>
#include <cassert>
#include <Processing.NDI.Advanced.h>
#include <iostream>
#include <pthread.h>
#include <cstring>

#include "pollarevma.h"


#ifdef _WIN32
#ifdef _WIN64
#pragma comment(lib, "Processing.NDI.Lib.Advanced.x64.lib")
#else // _WIN64
#pragma comment(lib, "Processing.NDI.Lib.Advanced.x86.lib")
#endif // _WIN64
#else

#endif // _WIN32

using namespace std;

// TODO change all printf to cout/cerr
static void *NDIStreamToSDL2(void *ndi_source) {
    auto *ndi_source_num = ((NDISourceNum *) ndi_source);
    auto *p_sources = ndi_source_num->p_sources;
    auto num = ndi_source_num->num;

    NDIlib_recv_create_v3_t recv_desc;
    recv_desc.color_format = NDIlib_recv_color_format_RGBX_RGBA;
    NDIlib_recv_instance_t pNDI_recv = NDIlib_recv_create_v3(&recv_desc);
    if (!pNDI_recv) {
        printf("!pNDI_recv, exiting ...\n");
        return nullptr;
    }

    // Connect to our sources
    NDIlib_recv_connect(pNDI_recv, &p_sources[num]);
    // Create an AV sync object
    NDIlib_avsync_instance_t pNDI_avsync = NDIlib_avsync_create(pNDI_recv);
    // We wait for up to a minute to receive a video frame
    NDIlib_video_frame_v2_t video_frame;
    NDIlib_audio_frame_v3_t audio_frame;
    NDIlib_audio_frame_v2_t audio_frame_v2;
    NDIlib_FourCC_video_type_e FourCC_video_type;
    SDLDisplay display = SDLDisplay();

    using namespace std::chrono;
    for (const auto start = high_resolution_clock::now(); true;) {
        switch (NDIlib_recv_capture_v2(pNDI_recv, &video_frame, nullptr, nullptr, 5000)) {
            case NDIlib_frame_type_status_change:
                printf("frame_type: NDIlib_frame_type_status_change\n");
                break;
            case NDIlib_frame_type_none:
                printf("frame_type: NDIlib_frame_type_none\n");
                break;
            case NDIlib_frame_type_video: {
//                printf("frame_type: NDIlib_frame_type_video\n");
                // If we have stride that does not match the width then we'd need to make a copy, but this is just test
                // code so this will have to do for now in this example.
                assert(video_frame.line_stride_in_bytes == video_frame.xres * 4);
                FourCC_video_type = video_frame.FourCC;

                switch (FourCC_video_type) {
                    case NDIlib_FourCC_type_UYVY: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_UYVY;
//                        cout << "NDIlib_FourCC_type_UYVY" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_UYVA: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_UNKNOWN;
//                        cout << "NDIlib_FourCC_type_UYVA" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_P216: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_UNKNOWN;
//                        cout << "NDIlib_FourCC_type_P216" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_PA16: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_UNKNOWN;
//                        cout << "NDIlib_FourCC_type_PA16" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_YV12: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_YV12;
//                        cout << "NDIlib_FourCC_type_YV12" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_I420: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_UNKNOWN;
//                        cout << "NDIlib_FourCC_type_I420" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_NV12: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_NV12;
//                        cout << "NDIlib_FourCC_type_NV12" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_BGRA: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_ARGB8888;
//                        cout << "NDIlib_FourCC_type_BGRA" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_BGRX: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_XRGB8888;
//                        cout << "NDIlib_FourCC_type_BGRX" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_RGBA: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_ABGR8888;
//                        cout << "NDIlib_FourCC_type_RGBA" << endl;
                    }
                        break;
                    case NDIlib_FourCC_type_RGBX: {
                        display.sdl_pixel_format = SDL_PIXELFORMAT_XBGR8888;
//                        display.sdl_pixel_format = SDL_PIXELFORMAT_RGBX8888;
//                        cout << "NDIlib_FourCC_type_RGBX" << endl;
                    }
                        break;
                    case NDIlib_FourCC_video_type_max: {

                    }
                        break;
                    default: {

                    }
                        break;
                }

                if (!display.isRunning) {
                    display.isRunning = display.init_display(video_frame.xres, video_frame.yres,
                                                             (uint32_t *) (video_frame.p_data),
                                                             p_sources[num].p_ndi_name);
                    if (!display.isRunning) {
                        printf("Failed to init display!\n");
                        exit(-1);
                    } else {
                        printf("SDL Display (%s) is initialized...\n", p_sources[num].p_ndi_name);
                    }
                }
                if (NDIlib_avsync_synchronize(pNDI_avsync, &video_frame, &audio_frame) >= NDIlib_avsync_ret_success) {
//                    printf("Video data received (%dx%d, %1.2f Hz) with audio (%d samples, %d channels, %d Hz).\n",
//						video_frame.xres, video_frame.yres, (float)(video_frame.frame_rate_N / video_frame.frame_rate_D),
//						audio_frame.no_samples, audio_frame.no_channels, audio_frame.sample_rate );
                    NDIlib_audio_frame_interleaved_16s_t audio_frame_16bpp_interleaved;
                    audio_frame_16bpp_interleaved.reference_level = 0;    // 0dB
                    audio_frame_16bpp_interleaved.p_data = new short[audio_frame.no_samples * audio_frame.no_channels];
                    // Convert it to v2
                    audio_frame_v2.sample_rate = audio_frame.sample_rate;
                    audio_frame_v2.no_channels = audio_frame.no_channels;
                    audio_frame_v2.no_samples = audio_frame.no_samples;
                    audio_frame_v2.timecode = audio_frame.timecode;
                    audio_frame_v2.p_data = reinterpret_cast<float *>(audio_frame.p_data);
                    audio_frame_v2.channel_stride_in_bytes = audio_frame.channel_stride_in_bytes;
                    audio_frame_v2.p_metadata = audio_frame.p_metadata;
                    audio_frame_v2.timestamp = audio_frame.timestamp;
                    // Convert it to AUDIO_S16
                    NDIlib_util_audio_to_interleaved_16s_v2(&audio_frame_v2, &audio_frame_16bpp_interleaved);
                    if (!isAudioRunning) {
                        SDLDisplay::init_audio(audio_frame_16bpp_interleaved.sample_rate,
                                               audio_frame_16bpp_interleaved.no_channels,
                                               audio_frame_16bpp_interleaved.no_samples);
                    }

                    if (SDL_QueueAudio(audio_dev, (void *) audio_frame_16bpp_interleaved.p_data,
                                       audio_frame_16bpp_interleaved.no_samples * 4) < 0)
                        std::cerr << "Error on SDL_QueueAudio" << std::endl;
                    // TODO Free new buffer from SDL_MixAudioFormat

                    // Free the original buffer
                    NDIlib_recv_free_audio_v3(pNDI_recv, &audio_frame);

                    // Free the interleaved audio data
                    delete[] audio_frame_16bpp_interleaved.p_data;
                } else {
                    // We got video without audio
//					printf(
//						"Ignoring Video data received (%dx%d, %1.2f Hz) without audio.\n",
//						video_frame.xres, video_frame.yres, (float)(video_frame.frame_rate_N / video_frame.frame_rate_D)
//					);
                }
                //TODO This works solid so far but we need to add code
                //     that detects the change in resolution and update the SDL window
                //     Wait until we hit this in testing - SDL might just handle it
                display.update_display((uint32_t *) (video_frame.p_data), video_frame.xres, video_frame.yres);

                // Free the data
                NDIlib_recv_free_video_v2(pNDI_recv, &video_frame);
                if (display.shutdown_thread)
                    return nullptr;
            }
                break;
            case NDIlib_frame_type_audio:
                break;
            case NDIlib_frame_type_metadata:
                printf("frame_type: NDIlib_frame_type_metadata\n");
                break;
            case NDIlib_frame_type_error:
                printf("frame_type: NDIlib_frame_type_error\n");
                break;
            default:
                printf("unknown frame_type\n");
        }
    }

    // TODO You need to get this code into a catch somehow... something that is called before quitting or being killed
    // Destroy the receiver
    printf("Destroying receiver...\n");
    NDIlib_recv_destroy(pNDI_recv);
    display.OnExit();
    return nullptr;
}

// Main should get list of streams names it wants and then look for them
// For each stream found create a thread
//    In that thread it should create a NDI receiver and if applicable an SDL display
int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[]) {

    // TODO: add sighandlers
    // Not required, but "correct" (see the SDK documentation.)
    if (!NDIlib_initialize()) {
        printf("!NDIlib_initialize(), exiting ...\n");
        return 0;
    }

    // Create a finder
    NDIlib_find_instance_t pNDI_find = NDIlib_find_create_v2();
    if (!pNDI_find) {
        printf("!pNDI_find, exiting ...\n");
        return 0;
    }
    // TODO print out display info: number of displays and integer ID of each
    //      this will also need to get the geomtetry of each to play windows on right screen before fullscreening
    //      This requires user input below on source interaction.. what screen to use for what source
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        printf("%s%s\n", __FILE__, " SDL_Init() fail!");
        return 0;
    }

    // Search network for NDI sources
    uint32_t no_sources = 0;
    const NDIlib_source_t *p_sources = nullptr;
    //FIXME: make it ask user for the number of source and then it will wait until it finds that many sources below
    uint32_t target_num_sources = 1;
    while (no_sources < target_num_sources) {
        // Wait until the sources on the network have changed
        printf("Looking for sources ...\n");
        NDIlib_find_wait_for_sources(pNDI_find, 1000/* One second */);
        p_sources = NDIlib_find_get_current_sources(pNDI_find, &no_sources);
    }
    printf("Found %d source(s)...\n", no_sources);
    for (int i = 0; i < no_sources; ++i) {
        printf("SOURCE %d: |%s| at address %s, creating receiver ...\n", i, p_sources[i].p_ndi_name,
               p_sources[i].p_ip_address);
    }
    auto *selected = new bool[no_sources];
    // Init selected all to false
    for (int i = 0; i < no_sources; i++) {
        selected[i] = false;
    }
    cout << "Enter source number to display: ";
    uint16_t source_select = 0;
    cin >> source_select;
    cout << endl;
    selected[source_select] = true;
    cout << "You have selected to display: " << p_sources[source_select].p_ndi_name << endl;
    if (no_sources > 1) {
        cout << "Would you like to display another source? [y/n]: ";
        string answer;
        cin >> answer;
        cout << endl;
        if (strcmp(answer.c_str(), "Y") == 0 || strcmp(answer.c_str(), "y") == 0) {
            for (int i = 0; i < no_sources; i++) {
                if (!selected[i]) {
                    printf("SOURCE %d: |%s| at address %s, creating receiver ...\n", i, p_sources[i].p_ndi_name,
                           p_sources[i].p_ip_address);
                }
            }
            cout << "Enter source number to display: ";
            cin >> source_select;
            cout << endl;
            selected[source_select] = true;
        }
    }

    auto *threads = new pthread_t[no_sources];
    auto *sn = new NDISourceNum[no_sources];
    void *thr_ret;
    for (int i = 0; i < no_sources; ++i) {
        printf("SOURCE %d: |%s| at address %s, creating receiver ...\n", i, p_sources[i].p_ndi_name,
               p_sources[i].p_ip_address);
        // TODO Ths is a temp hack to only allow one specific source... will make interactive via shell later
        if (selected[i]) {
            std::cout << "Starting Stream " << i << ": " << p_sources[i].p_ndi_name << std::endl;
            sn[i].p_sources = p_sources;
            sn[i].num = i;
            int ptc_ret = pthread_create(&(threads[i]), nullptr, NDIStreamToSDL2, &(sn[i]));
            if (ptc_ret != 0)
                std::cerr << "PTHREAD error!!!" << std::endl;
        }
    }

    // Rejoin threads
    for (int i = 0; i < no_sources; ++i) {
        if (selected[i])
            pthread_join(threads[i], &thr_ret);
    }
    delete[] selected;
    delete[] threads;
    delete[] sn;
    // Not required, but nice
    printf("Destroying NDILib...\n");
    NDIlib_destroy();

    // Finished
    printf("Exiting program...\n");
    return 0;
}
